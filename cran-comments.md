## R CMD check results
Duration: 1m 5.1s

0 errors ✔ | 0 warnings ✔ | 0 notes ✔

R CMD check succeeded

* This is a new release.

We obtain 2 NOTES from 'check_for_cran()'

One on "checking CRAN incoming feasibility ... NOTE
Possibly misspelled words in DESCRIPTION" must be maintained.

The latter on "checking examples" was not registerd by devtools::check()
